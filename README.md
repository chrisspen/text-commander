Text Commander
==========

A missile-commander style game to help practice memory.

Usage
-------

Checkout project.

Create `data/data.json` and populate it with a JSON hash like:

    {"foreign word": "English word"}

Run locally with:

    python -m http.server 8080

The visit:

    http://localhost:8080/index.html

Strings associated with the "foreign word" will then gradually fall down and you have to type the English word before they hit one of your cities.

As you progress, the rate of incoming words will increase.

The game ends when all your cities are destroyed.

Press `esc` to pause or unpause the game.
