#!.env/bin/python
"""
Converts an ODS spreadsheet containing a list of vocabulary entries,
with one column named "English" and the translation named "Other",
and generates an equivalent JSON file.
"""

import sys
import json
import pandas as pd

# Read the ODS spreadsheet into a pandas DataFrame
filename = sys.argv[1]
print('Converting %s.' % filename)
df = pd.read_excel(filename, engine='odf')

# Convert the DataFrame to a dictionary with "English" column values as keys and "Other" column values as values
data_dict = df.set_index('Other')['English'].to_dict()

# Write the dictionary to a JSON file
with open('data/output.json', 'w') as json_file:
    json.dump(data_dict, json_file, indent=4)
print('Done.')
